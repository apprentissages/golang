package main

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(handleRequest)
}

func handleRequest() (string, error) {
	r := map[string]interface{}{
		"value": "Hello Lambda",
	}

	output, _ := json.Marshal(r)
	return string(output), nil
}
